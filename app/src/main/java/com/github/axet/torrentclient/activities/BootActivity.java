package com.github.axet.torrentclient.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.github.axet.torrentclient.app.TorrentApplication;

public class BootActivity extends Activity {
    public static void createApplication(Context context) {
        Intent intent = new Intent(context, BootActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TorrentApplication.from(this).createThread(null);
        finish();
    }
}
